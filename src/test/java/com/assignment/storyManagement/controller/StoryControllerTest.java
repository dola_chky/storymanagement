package com.assignment.storyManagement.controller;

import com.assignment.storyManagement.model.Story;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;


@RunWith(SpringRunner.class)
@WebMvcTest(StoryController.class)
public class StoryControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private StoryController storyController;

    @Test
    public void getAllStories() throws Exception {
        Story story = new Story();
        story.setTitle("This is a test story");
        story.setStoryBody("once upon a time there lived a happy woman");
        story.setPublishedDate(new Date());
        story.setCreatedBy("Dola");

        List storyList = singletonList(story);

        given(storyController.getAllStories()).willReturn(storyList);
        mvc.perform(get("/api/stories")
                .with(user("admin").password("admin"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].title", is(story.getTitle())));
    }

    @Test
    public  void getStoryById() throws Exception {
        Story story = new Story();
        story.setTitle("This is a test story");
        story.setStoryBody("once upon a time there lived a happy woman");
        story.setPublishedDate(new Date());
        story.setCreatedBy("Dola");
        given(storyController.getStoryAsJson(story.getStoryId())).willReturn(story);
        mvc.perform(get("/api/story/"+story.getStoryId())
                .with(user("admin").password("admin"))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("title", is(story.getTitle())));

    }


}
